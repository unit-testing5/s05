//after installing express
//we are going to populate the server.js to include the route

const express = require('express');

const app = express();

const PORT = 5001;

app.use(express.json());

require('./app/routes')(app, {});

app.listen(PORT, () => {
	console.log('App is running on port ' + PORT);
})
